/* eslint-disable no-console */

const http = require('http');
const sio = require('socket.io');
const serveStatic = require('serve-static');
const finalHandler = require('finalhandler');

console.log('index.js start');

const vec3Zero = {
  x: 0,
  y: 0,
  z: 0
};

// makes dst take on the component values of src
const vec3TakeOn = (dst, src) => {
  if (dst === undefined) {
    dst = {};
  }
  /* eslint-disable no-param-reassign */
  dst.x = src.x;
  dst.y = src.y;
  dst.z = src.z;
  /* eslint-enable no-param-reassign */
  return dst;
};

const theBoss = {
  pos: vec3TakeOn({}, vec3Zero),
  rot: vec3TakeOn({}, vec3Zero),
  health: 10,
  guid: '',
  socket: {},
};
// map of player GUIDs to { pos: {x,y,z}, rot: {x,y,z}, health }
// TODO: figure out whether we really need to maintain a collection of players,
// since socket.broadcast.emit takes care of player enumeration, and the
// sendingPlayer closure provides access to the current player.
const allPlayers = new Map();

const serveStaticFile = serveStatic('../fighter-client/', {});

const httpServer = http.createServer((req, res) => {
  serveStaticFile(req, res, finalHandler(req, res));
});

/**
 * SOCKET.IO SETUP
 */
const sioServer = sio(httpServer, {});

const generateGUID = (playMode) => {
  if (playMode == 'boss') {
    return 'boss';
  }
  // eslint-disable-next-line no-bitwise
  const S4 = () => (((1 + Math.random()) * 0x10000) | 0).toString(16).substring(1);
  // return (S4() + S4() + "-" + S4() + "-" + S4() + "-" + S4() + "-" + S4() + S4() + S4());
  return S4() + S4();
};

let numClients = 0;
sioServer.on('connection', (socket) => {
  // Generate random client ids upon user join.
  // TODO: generate random position to place them at.
  let playMode = socket.handshake.query.mode;
  let connectionId = generateGUID(playMode);

  // the player who just connected
  const sendingPlayer = {
    pos: vec3TakeOn({}, vec3Zero),
    rot: vec3TakeOn({}, vec3Zero),
    health: 2,
  };
  allPlayers[connectionId] = sendingPlayer;
  numClients += 1;
  console.log(`* connection * : #${connectionId} established. ${numClients} clients.`);

  // Tell the player their id.
  socket.emit('gentoken', connectionId);
  // List currently active players, that they may populate the scene.
  socket.emit('enumeratePlayers_forClient', allPlayers);

  socket.on('bossHit', () => {
    console.log('boss was hit!');
    theBoss.health -= 1;
    socket.broadcast.emit('bossHit_forClients', { health: theBoss.health });
  });

  socket.on('bossDied', () => {
    console.log('boss is dead!');
    // TODO: tell clients that the boss died when boss health becomes 0 after a bossHit,
    // instead of when one of the clients decides that the boss has died...
    theBoss.health = 0;
    socket.broadcast.emit('bossDied_forClients');
  });

  // Broadcast one fighter's movement to all other players.
  socket.on('entityMovement', (elementId, location) => {
    if (elementId == 'cam') {
      vec3TakeOn(sendingPlayer.location, location);
      socket.broadcast.emit('playerMove_forClients', {
        id: connectionId,
        location, // javascript shorthand for "location: location"
      });
    } else {
      socket.broadcast.emit('handMove_forClients', elementId, location);
    }
  });

  socket.on('bossJoin', (oldId) => {
    console.log(`bossJoin : ${oldId} => 'boss'`);
    // The boss has joined. Let's make their id 'boss'.
    allPlayers.delete(oldId);
    connectionId = 'boss';
    allPlayers[connectionId] = sendingPlayer;

    socket.broadcast.emit('fighterQuit_forClients', oldId);
    socket.broadcast.emit('playerJoin_forClients', 'boss', {
      pos: sendingPlayer.pos,
      rot: sendingPlayer.rot,
    });
  });

  socket.on('bossMove', (playerKey, location) => {
    vec3TakeOn(theBoss.location, location);
    socket.broadcast.emit('bossMove_forClients', location);
  });

  socket.on('fighterShoot', (location) => {
    // We'll decide if we want to render this client or server side...
    socket.broadcast.emit('fighterShoot_forClients', connectionId);
  });

  socket.on('bossShoot', (hand) => {
    console.log('bossShoot');
    socket.broadcast.emit('bossShoot_forClients', hand);
  });

  socket.on('disconnect', () => {
    console.log(`disconnect : from user ${connectionId}`);
    allPlayers.delete(connectionId);
    numClients -= 1;
    console.log(`socket.io connection #${connectionId} lost. ${numClients} clients.`);
    socket.broadcast.emit('fighterQuit_forClients', connectionId);
  });

  // tell the other clients about the new client
  socket.broadcast.emit('playerJoin_forClients', connectionId, {
    pos: sendingPlayer.pos,
    rot: sendingPlayer.rot,
  });
});

httpServer.listen(3000);
